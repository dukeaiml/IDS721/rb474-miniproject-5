# rb474-miniproject-5

## Service
The Lambda function returns the population in the United States for a given year if such information is present in the database. The function is invoked by Gateway. You can follow the [link](https://ardn6nyp53.execute-api.us-west-1.amazonaws.com/population?year=2018) to test its functionality.

## Process
1. `cargo lambda new`
2. Change main.rs and Cargo.toml for the function to return population for a given year.
3. Create DynamoDB.
4. Create a user with the policies required to deploy the function and access the database. The keys are to be provided in `aws configure`.
5. Build and deploy.

## Screenshots
### Example of usage
![](images/example.png)

### Function overview
![](images/function_overview.png)